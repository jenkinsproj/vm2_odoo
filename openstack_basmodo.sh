#!/bin/bash

ssh-keygen -q -N ""  -f open_key
openstack keypair create --public-key open_key.pub sto4_key
openstack keypair list

for machine in `echo vm1 vm2`; do

    echo "Debut creation VM $machine"

    openstack server create --network private --key-name sto4_key --flavor ds512M --image ubuntu $machine

    openstack floating ip create public

    foo=`openstack floating ip list | egrep -e "None.*\|.*None"`

    if [ ! -z "$foo" ]; then

        #$value= echo "$foo" | head -n1 | awk '{print $1;}'
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante
        echo "Floating IP = $bar"
    else
        echo "Error"
        exit 1
    fi

    case $machine in

        vm1)
            ip_vm1=$bar; echo $ip_vm1;;

        vm2)
            ip_vm2=$bar; echo $ip_vm2;;
        # vm3)
        #     $ip_vm3 = $bar;;
        # vm4)
        #     $ip_vm4 = $bar;;
        # vm5)
        #     $ip_vm5 = $bar;;
        *)
            echo "Error";;
    esac

    openstack server add floating ip $machine $bar
   echo "Je dors pendant 20s"
   sleep 38

  # controle du demarrage instance
    # a=`openstack server list --name ${machine} --status ACTIVE`
    # echo "$a++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!!"
    # while [ -z "$a" ]; do
    #   echo "Please wait"
    #   sleep 1
    #   a=`openstack server list --name ${machine} --status ACTIVE`
    # done
    echo "ubuntu@$bar"
     sshpass -f password.txt ssh-copy-id  -i ./open_key.pub root@"$bar"
    #ssh -o "StrictHostKeyChecking no" root@$bar
     ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar
    #ssh -o "StrictHostKeyChecking no" -o PasswordAuthentication=no $bar
    # /usr/bin/sshpass -f ./password.txt /usr/bin/ssh-copy-id -o StrictHostKeyChecking=no -i ./open_key.pub root@"$bar"
    # sshpass -f password.txt ssh -o StrictHostKeyChecking=no root@${bar}
    #sshpass -f password.txt ssh-copy-id  -i ./open_key.pub root@"$bar"

    echo "Fin de la creation VM $machine"
done
   
    echo "Je dors pendant 45s"
    sleep 45


for machine in `echo vm1 vm2 `; do

    case $machine in

        vm1)
            #source script/installation.sh
           echo "Installation Apache2"
           ssh -i open_key ubuntu@$ip_vm1 "sudo sed -i '$ a nameserver 8.8.8.8  nameserver 8.8.4.4' /etc/resolv.conf;sudo apt update ; sudo apt install -y apache2; sudo systemctl start  apache2 ;systemctl enable apache2";;

        vm2)
           echo  "installation odoo"
           ssh -i open_key ubuntu@$ip_vm2 "sudo sed -i '$ a nameserver 8.8.8.8 nameserver 8.8.4.4' /etc/resolv.conf";
           sudo apt-get update &&  sudo apt upgrade -y;
           #scp -i open_key odoo-install.sh  ubuntu@172.24.10.184:/home/ubunu
           #scp -i open_key ./odoo-install.sh -f ubuntu@$ip_vm2:/home/ubuntu;
           sudo apt install git -y ;
           sudo git clone https://gitlab.com/jenkinsproj/vm2_odoo.git;
           ssh -i open_key ubuntu@$ip_vm2 "sudo chmod a+x /home/ubuntu/vm2_odoo/odoo-install.sh";
           ssh -i open_key ubuntu@$ip_vm2 "sudo sed -i '$ a nameserver 8.8.8.8 nameserver 8.8.4.4' /etc/resolv.conf; sudo apt install -y nginx ;sudo systemctl start nginx";;
           sudo mv /home/ubuntu/vm2_odoo/default /etc/nginx/sites-available/;
           sudo systemctl restart nginx;
           source /ubuntu/vm2_odoo/odoo-install.sh;
           sudo systemctl start odoo;;

        *)

           echo "Unknown" ;;

    esac

done
